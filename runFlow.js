request = require('request');

var argv = require('minimist')(process.argv.slice(2));
baseUrl		= argv._[0];
user  		= argv._[1];
pass  		= argv._[2];
uuid        = argv._[3];
inputs	    = argv._[4];
runName	    = argv._[5];

var runFlow = {};

runFlow.startFlowSync = function (ooConn , uuid , inputs, runName) {
	return new Promise(function(resolve , reject){ 

		var options = {
			url: ooConn.baseUrl + "/oo/rest/latest/executions",
			method: "POST",
			body: {
				uuid: uuid,
				runName: runName,
				inputs: inputs
			},
			json: true,
			strictSSL: false,
			auth: {
				user: ooConn.user,
		    	password: ooConn.pass
		  	}
		};

		req = request (options, function (err, httpResponse , body) {
			if (err) {
				reject(err)
			} else {
				resolve(body)
			}
		})
	})
}

if (baseUrl) {

	var ooConn = {
		baseUrl : baseUrl,
		user: user,
		pass: pass
	}

	runFlow.startFlowSync(ooConn, uuid , JSON.parse(inputs), runName)
	.then(function(a){
		debugger;
		console.log(chalk.green('success\n\n') , a);

	},function(err){
		debugger;
		console.log(chalk.red('failed') , err);

	})
}

module.exports = runFlow;
